// c printf
#include <cstdio>
// plugin_name_args & plugin_info
#include <gcc-plugin.h>
// pass_data
#include <tree-pass.h>
// 'g' global variable
#include <context.h>

// our pass
#include "mpi_collective.hpp"

/* Global variable required for plugin to execute */
int plugin_is_GPL_compatible;

void print_plugin_infos(struct plugin_name_args *plugin_info,
                        struct plugin_gcc_version *version)
{
	printf("plugin_init: Entering...\n");
	printf("\tbasever = %s\n", version->basever);
	printf("\tdatestamp = %s\n", version->datestamp);
	printf("\tdevphase = %s\n", version->devphase);
	printf("\trevision = %s\n", version->revision);
	printf("\tconfig = %s\n", version->configuration_arguments);

	printf("\tbase_name = %s\n", plugin_info->base_name);
	printf("\tfull_name = %s\n", plugin_info->full_name);
	for (int i = 0; i < plugin_info->argc; ++i)
		printf("\t\targ %d: %s = %s\n", i, plugin_info->argv[i].key,
		       plugin_info->argv[i].value);
	printf("\tversion = %s\n", plugin_info->version);
	printf("\thelp = %s\n", plugin_info->help);
	printf("\n\n");
}

vec<tree> *fun_vec;
vec<location_t> *loc_vec;

bool dumpgraph;
bool showinfo;


/* Main entry point for plugin */
int
plugin_init(struct plugin_name_args *plugin_info,
            struct plugin_gcc_version *version)
{
	dumpgraph = false;
	showinfo = false;

	for(int i = 0; i < plugin_info->argc ; ++i)
	{
		if(strcmp(plugin_info->argv[i].key, "dumpgraph") == 0){
			dumpgraph = true;
		}
		if(strcmp(plugin_info->argv[i].key, "showinfo") == 0){
			showinfo = true;
		}
	}
	
	if (showinfo)
	{
		print_plugin_infos(plugin_info, version);
	}

	pass_mpi_collective p(g);

	fun_vec = new vec<tree>;
	loc_vec = new vec<location_t>;

	fun_vec->create(0);
	loc_vec->create(0);
	// RELEASE in verify_mpi_colllist at PLUGIN FINISH UNIT

	struct register_pass_info pass_info;

	pass_info.pass = &p;
	pass_info.reference_pass_name = "cfg";
	pass_info.ref_pass_instance_number = 0;
	pass_info.pos_op = PASS_POS_INSERT_AFTER;

	register_callback(plugin_info->base_name,
	                  PLUGIN_PASS_MANAGER_SETUP,
	                  NULL,
	                  &pass_info);

	register_callback(plugin_info->base_name,
	                  PLUGIN_PRAGMAS,
	                  register_mpicoll_check_pragma,
	                  NULL);

	register_callback(plugin_info->base_name,
	                  PLUGIN_FINISH_UNIT,
	                  verify_mpicoll_list,
	                  NULL);

	return 0;
}
