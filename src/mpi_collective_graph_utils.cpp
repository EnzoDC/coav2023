#include <list>

#include "mpi_collective.hpp"

// bb aux alloc, free & reset
void pass_mpi_collective::alloc_bb_aux(function *fun)
{
	basic_block bb;

	FOR_ALL_BB_FN(bb, fun)
	{
		bb->aux = (bb_data *) new bb_data();
		bb_data *data = ((bb_data *) bb->aux);
		data->mpi_code = LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
		bitmap_initialize(&data->dom, &bitmap_default_obstack);
		bitmap_initialize(&data->post_dom, &bitmap_default_obstack);
		bitmap_initialize(&data->dom_front, &bitmap_default_obstack);
		bitmap_initialize(&data->post_dom_front, &bitmap_default_obstack);
		for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; ++i)
		{
			data->collective_rank[i] = 0;
		}
		bitmap_initialize(&data->seens, &bitmap_default_obstack);
	}

	reset_bb_mark(fun);
}

void pass_mpi_collective::free_bb_aux(function *fun)
{
	basic_block bb;
	FOR_ALL_BB_FN(bb, fun)
	{
		if (bb->aux != NULL)
		{
			bb_data *data = ((bb_data *) bb->aux);
			bitmap_clear(&data->dom);
			bitmap_clear(&data->post_dom);
			bitmap_clear(&data->dom_front);
			bitmap_clear(&data->post_dom_front);
			bitmap_clear(&data->seens);
			delete (bb_data *) bb->aux;
			bb->aux = NULL;
		}
	}
}

void pass_mpi_collective::reset_bb_mark(function *fun)
{
	basic_block bb;

	FOR_ALL_BB_FN(bb, fun)
	{
		((bb_data *) bb->aux)->mark1 = 0;
		((bb_data *) bb->aux)->mark2 = 0;
	}
}

// edge aux alloc & free
void pass_mpi_collective::alloc_edge_aux(function *fun)
{
	basic_block bb;
	edge e;
	edge_iterator ei;

	FOR_ALL_BB_FN(bb, fun)
	{
		FOR_EACH_EDGE(e, ei, bb->succs)
		{
			e->aux = new edge_data();
		}
	}
}

void pass_mpi_collective::free_edge_aux(function *fun)
{
	basic_block bb;
	edge e;
	edge_iterator ei;

	FOR_ALL_BB_FN(bb, fun)
	{
		FOR_EACH_EDGE(e, ei, bb->succs)
		{
			if (e->aux != NULL)
			{
				delete (edge_data *) e->aux;
				e->aux = NULL;
			}
		}
	}
}

// loop detection
void pass_mpi_collective::mark_edge(function *fun)
{
	mark_edge(ENTRY_BLOCK_PTR_FOR_FN(fun));
	reset_bb_mark(fun);
}

void pass_mpi_collective::mark_edge(basic_block bb)
{
	edge e;
	edge_iterator ei;

	((bb_data *) bb->aux)->mark1 = 1;
	bitmap_set_bit(&((bb_data *) bb->aux)->seens, bb->index);
	FOR_EACH_EDGE(e, ei, bb->succs)
	{
		if (((edge_data *) e->aux)->loop)
		{
			continue;
		}
		if (bitmap_bit_p(&((bb_data *) bb->aux)->seens, e->dest->index))
		{
			((edge_data *) e->aux)->loop = true;
		}
		if (! ((bb_data *) e->dest->aux)->mark1)
		{
			bitmap_ior_into(&((bb_data *) e->dest->aux)->seens,
			                &((bb_data *) bb->aux)->seens);
			mark_edge(e->dest);
		}
	}
}
