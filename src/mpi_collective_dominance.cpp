#include <gcc-plugin.h>

#include "mpi_collective.hpp"

// calculate dominances and post dominances informations in gcc.
void pass_mpi_collective::calc_dom_data()
{
	calculate_dominance_info(CDI_DOMINATORS);
	calculate_dominance_info(CDI_POST_DOMINATORS);
}

// free dominances and post dominancse informations in gcc.
void pass_mpi_collective::free_dom_data()
{
	free_dominance_info(CDI_DOMINATORS);
	free_dominance_info(CDI_POST_DOMINATORS);
}

// set dominances informations from gcc in bb.
void pass_mpi_collective::label_dom(function *fun)
{
	basic_block bb;
	auto_vec<basic_block> dom_gccvec;
	size_t size_dom;

	FOR_ALL_BB_FN(bb, fun)
	{
		bb_data *data = ((bb_data *) bb->aux);

		dom_gccvec = get_all_dominated_blocks(CDI_DOMINATORS, bb);
		size_dom = dom_gccvec.length();
		for (size_t i = 0; i < size_dom; ++i)
		{
			if (dom_gccvec[i]->index != bb->index)
			{
				bitmap_set_bit(&data->dom, dom_gccvec[i]->index);
			}
		}
	}
}

// set post dominances informations from gcc in bb.
void pass_mpi_collective::label_post_dom(function *fun)
{
	basic_block bb;
	auto_vec<basic_block> post_dom_gccvec;
	size_t size_post_dom;

	FOR_ALL_BB_FN(bb, fun)
	{
		bb_data *data = ((bb_data *) bb->aux);

		post_dom_gccvec = get_all_dominated_blocks(CDI_POST_DOMINATORS, bb);
		size_post_dom = post_dom_gccvec.length();
		for (size_t i = 0; i < size_post_dom; ++i)
		{
			if (post_dom_gccvec[i]->index != bb->index)
			{
				bitmap_set_bit(&data->post_dom, post_dom_gccvec[i]->index);
			}
		}
	}
}

// calculate dominances frontiers informations
// and save them in bb.
void pass_mpi_collective::label_dom_front(function *fun)
{
	edge p;
	edge_iterator ei;
	basic_block bb;
	basic_block dom_bb;
	basic_block runner;
	bb_data *data;

	FOR_EACH_BB_FN(bb, fun)
	{
		if (EDGE_COUNT(bb->preds) < 2)
		{
			continue;
		}
		dom_bb = get_immediate_dominator(CDI_DOMINATORS, bb);
		FOR_EACH_EDGE(p, ei, bb->preds)
		{
			runner = p->src;
			if (runner == ENTRY_BLOCK_PTR_FOR_FN(fun))
			{
				continue;
			}
			while (runner != dom_bb)
			{
				data = (bb_data *) runner->aux;
				if (bitmap_bit_p(&data->dom_front, bb->index))
				{
					break;
				}
				bitmap_set_bit(&data->dom_front, bb->index);
				runner = get_immediate_dominator(CDI_DOMINATORS, runner);
			}
		}
	}
}

// calculate post dominances frontiers informations
// and save them in bb.
void pass_mpi_collective::label_post_dom_front(function *fun)
{
	edge p;
	edge_iterator ei;
	basic_block bb;
	basic_block dom_bb;
	basic_block runner;
	bb_data *data;

	FOR_EACH_BB_FN(bb, fun)
	{
		if (EDGE_COUNT(bb->succs) < 2)
		{
			continue;
		}
		dom_bb = get_immediate_dominator(CDI_POST_DOMINATORS, bb);
		FOR_EACH_EDGE(p, ei, bb->succs)
		{
			runner = p->dest;
			if (runner == EXIT_BLOCK_PTR_FOR_FN(fun))
			{
				continue;
			}
			while (runner != dom_bb)
			{
				data = (bb_data *) runner->aux;
				if (bitmap_bit_p(&data->post_dom_front, bb->index))
				{
					break;
				}
				bitmap_set_bit(&data->post_dom_front, bb->index);
				runner = get_immediate_dominator(CDI_POST_DOMINATORS, runner);
			}
		}
	}
}

// calculate post dominances of a set of node in bitmap nodes and put
// the result in pds.
// Take account of looping edge if it was previously calculated.
void pass_mpi_collective::__is_post_dom_bitmap(function *fun,
        basic_block bb, bitmap nodes, bitmap pds)
{
	edge e;
	edge_iterator ei;
	domination_status bb_dom_status;
	domination_status cursor_dom_status;

	//printf("entered bb->index : %i\n",bb->index);
	((bb_data *) bb->aux)->mark1 = 1; //node has been visited

	if (bitmap_bit_p(nodes, bb->index))
	{
		bitmap_set_bit(pds, bb->index);
		((bb_data *) bb->aux)->dom_status = DOMINATED;
		return;
	}

	if (EXIT_BLOCK_PTR_FOR_FN(fun) == bb)
	{
		((bb_data *) bb->aux)->dom_status = NOT_DOMINATED;
		return;
	}

	// in case we are in a leaf it will return NOT_APPLICABLE
	bb_dom_status = NOT_APPLICABLE;

	FOR_EACH_EDGE(e, ei, bb->succs)
	{
		if (((edge_data *) e->aux)->loop)
		{
			//printf("loop detected... Jumping out...\n");
			continue; //if marked as a loop or already visited, ignore
		}

		if (! ((bb_data *) e->dest->aux)->mark1)
		{
			__is_post_dom_bitmap(fun, e->dest, nodes, pds);
		}

		cursor_dom_status = ((bb_data *) e->dest->aux)->dom_status;

		if (cursor_dom_status == NOT_DOMINATED)
		{
			bb_dom_status = NOT_DOMINATED;
		}
		else if (cursor_dom_status == DOMINATED
		         && bb_dom_status != NOT_DOMINATED)
		{
			bb_dom_status = DOMINATED;
		}
	}

	if (bb_dom_status == DOMINATED)
	{
		// add the bb to the pdset if it is post dominated by set
		bitmap_set_bit(pds, bb->index);
	}

	((bb_data *) bb->aux)->dom_status = bb_dom_status;
}

// wrapper of __is_post_dom_bitmap
void pass_mpi_collective::get_post_dom_bitmap(function *fun,
        bitmap nodes, bitmap pds)
{
	__is_post_dom_bitmap(fun, ENTRY_BLOCK_PTR_FOR_FN(fun),
	                     nodes, pds);
	reset_bb_mark(fun);
}

// calculate post dominantion frontier of a post dominated set of nodes
// in pds and put the result in pdf.
void pass_mpi_collective::get_post_dom_frontier(function *fun,
        bitmap pds, bitmap pdf)
{
	//we check each bb, if a bb is in the postdom set, we check its
	//most adjacents preds. If a pred is not in the postdom set, it
	//is a elm of the frontier of set.
	basic_block bb;
	edge e;
	edge_iterator ei;

	FOR_ALL_BB_FN(bb, fun)
	{
		if (bitmap_bit_p(pds, bb->index))
		{
			//printf("entering %i\n", bb->index);
			FOR_EACH_EDGE(e, ei, bb->preds)
			{
				//printf("\tchecking out my parent %i\n", e->src->index);
				if (! ((edge_data *) e->aux)->loop
				    && !bitmap_bit_p(pds, e->src->index))
				{
					bitmap_set_bit(pdf, e->src->index);
				}
			}
		}
	}
}
