// std::max
#include <algorithm>
#include <stack>

#include "mpi_collective.hpp"

// dependance of context.h
#include <gcc-plugin.h>
// 'g' global variable
#include <context.h>
// dependence of gimple.h
#include <tree.h>
//gimple iterator dependence
#include <gimple.h>
// gsi
#include <gimple-iterator.h>
//error formating gcc-like
#include <diagnostic.h>

//wrapper function
void pass_mpi_collective::rank_collective(function *fun)
{
	//we calculate collective rank starting from the bottom
	__rank_collective(EXIT_BLOCK_PTR_FOR_FN(fun));
	reset_bb_mark(fun);
}

//recursive function calculating the rank for each mpi collective.
void pass_mpi_collective::__rank_collective(basic_block bb)
{
	edge e;
	edge_iterator ei;

	((bb_data *) bb->aux)->mark1 = 1; //we visited this node
	FOR_EACH_EDGE(e, ei, bb->preds)
	{
		if (((edge_data *) e->aux)->loop)//loop in the graph ; do not compute
		{
			continue;
		}

		if (! ((bb_data *) e->src->aux)->mark1) //unvisited node
		{
			__rank_collective(e->src); //recursion on parent nodes
		}

		//(note that entering this for loop imply we already processed our
		//parent node)
		for (int mpi_code = 0;
		     mpi_code < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
		     ++mpi_code)
			//the rank is per collective, thus we test on each mpicode.
		{
			((bb_data *) bb->aux)->collective_rank[mpi_code] =
			    std::max(((bb_data *) bb->aux)->collective_rank[mpi_code],
			             ((bb_data *) e->src->aux)->collective_rank[mpi_code]);
			if (((bb_data *) bb->aux)->mpi_code == mpi_code)
			{
				//if we are a new collective of this mpi_code,
				//then we are in a new rank;
				//we increment to reflect that.
				((bb_data *) bb->aux)->collective_rank[mpi_code]++;
				//then we update the global max to be sure
				collective_max_rank[mpi_code] =
				    std::max(collective_max_rank[mpi_code],
				             ((bb_data *) bb->aux)->collective_rank[mpi_code]);
				//printf("collective max rank for %i up to %i\n",
				//		 mpi_code, collective_max_rank[mpi_code]);
			}
		}
	}
}

//set in a bitmap the index of a bb that have the same rank for a collective
void pass_mpi_collective::get_mpi_coll_rank(function *fun,
        int rank, int mpi_code, bitmap mpi_coll)
{
	basic_block bb;

	FOR_ALL_BB_FN(bb, fun)
	{
		if (((bb_data *) bb->aux)->mpi_code == mpi_code
		    && ((bb_data *) bb->aux)->collective_rank[mpi_code] == rank)
		{
			//printf("bb %i is coll %i of rank %i\n",
			//	bb->index, mpi_code, rank);
			bitmap_set_bit(mpi_coll, bb->index);
		}
	}
}

//get first gimple statement in bb
gimple *pass_mpi_collective::get_first_stmt(basic_block bb)
{
	return bb->il.gimple.seq;
}

//get gimple statement in bb that is its mpi call
//This obv only works with bb that have a mpi call inside it.
gimple *pass_mpi_collective::get_mpi_stmt(basic_block bb)
{
	gimple_stmt_iterator gsi;
	gimple *stmt;

	for (gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi))
	{
		stmt = gsi_stmt(gsi);
		if (is_mpi_collec(stmt) == ((bb_data *) bb->aux)->mpi_code)
		{
			return stmt;
		}
	}
	//should not happen
	printf("ARGSH");
	return NULL;
}

//get gimple statement in bb that is its mpi call
//This obv only works with bb that have a mpi call inside it.
gimple *pass_mpi_collective::get_first_non_mpi_stmt(basic_block bb)
{
	gimple_stmt_iterator gsi;
	gimple *stmt;

	for (gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi))
	{
		stmt = gsi_stmt(gsi);
		if (!is_mpi_func(stmt))
		{
			return stmt;
		}
	}
	//should not happen
	printf("ARGSH");
	return NULL;
}

void pass_mpi_collective::raise_warning_mpi_rank(function *fun)
{
	int mpi_code;

	bitmap_head pds_head; //postdominance set (pds)
	bitmap_head pdf_head; //postdominance frontier (pdf)
	bitmap_head nodes_head;

	bitmap pds = &pds_head;
	bitmap pdf = &pdf_head;
	bitmap nodes = &nodes_head;
	bitmap tmp = NULL;

	bitmap_initialize(pds, &bitmap_default_obstack);
	bitmap_initialize(pdf, &bitmap_default_obstack);
	bitmap_initialize(nodes, &bitmap_default_obstack);

	bitmap_iterator bi;
	unsigned int index;

	for (mpi_code = 0; mpi_code < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
	     ++mpi_code)
	{
		//for a given set of nodes of of same mpi collective and rank,
		//we calculate its post-dominance frontier.
		//A Warning occurs when its postdominance frontier is not empty :
		//indeed, this mean that some path in our cfg does not necessarily go
		//through this collective, which means, some of our MPI threads does not
		//go through the same mpi collective at the same moment => deadlocks
		for (int rank = 1 ; rank < collective_max_rank[mpi_code] + 1 ; rank++)
		{
			//printf("mpi_code: %i at rank: %i\n", mpi_code, i);
			// DONE: split in 2, get mpi set & find set post dom
			get_mpi_coll_rank(fun, rank, mpi_code, nodes);
			get_post_dom_bitmap(fun, nodes, pds);
			get_post_dom_frontier(fun, pds, pdf);

			//printf("----\n");
			//debug_bitmap(nodes);
			//debug_bitmap(pds);
			//debug_bitmap(pdf);

			//indexes of bb forming the pdf
			std::stack<int> bb_pdf_indx_stack;
			//indexes of bb doing the mpi call
			std::stack<int> bb_call_indx_stack;
			if (! bitmap_empty_p(pdf))
			{
				//printf("MPI Collective %s incomplete rank at bbs: ",
				//		 mpi_collective_name[mpi_code]);

				//we add every call to this mpi collective in our stack.
				EXECUTE_IF_SET_IN_BITMAP(nodes, 0, index, bi)
				{
					bb_call_indx_stack.push(index);
					//printf("%i, ", index);
				}
			}

			while (! bitmap_empty_p(pdf))
			{
				//we stack the iterative pdf into an other stack.
				EXECUTE_IF_SET_IN_BITMAP(pdf, 0, index, bi)
				{
					bb_pdf_indx_stack.push(index);
					//printf("Divergence from bb %i\n", index);
				}

				bitmap_clear(nodes);
				bitmap_clear(pds);

				// swap nodes & pdf
				{
					tmp = nodes;
					nodes = pdf;
					pdf = tmp;
				}
				get_post_dom_bitmap(fun, nodes, pds);
				get_post_dom_frontier(fun, pds, pdf);

				//printf("----\n");
				//debug_bitmap(nodes);
				//debug_bitmap(pds);
				//debug_bitmap(pdf);
			}
			bitmap_clear(nodes);
			bitmap_clear(pds);
			bitmap_clear(pdf);

			//write the error message if needed by processing the stack
			if (!bb_pdf_indx_stack.empty())
			{
				location_t loc;
				//first divergence possible
				loc = gimple_location(
				          get_first_non_mpi_stmt(
				              get_bb_from_index(fun, bb_pdf_indx_stack.top())));
				warning_at(loc
				           , 0
				           , "The MPI Collective %<%s%>(%i) might be "
				           "unmatched in some MPI processes,"
				           " diverging from here:"
				           , mpi_collective_name[mpi_code], rank);
				bb_pdf_indx_stack.pop();

				while (!bb_call_indx_stack.empty())
				{
					loc = gimple_location(
					          get_mpi_stmt(
					              get_bb_from_index(
					                  fun, bb_call_indx_stack.top())));
					inform (loc, "MPI collective %<%s%>(%i) called here."
					        , mpi_collective_name[mpi_code], rank);
					bb_call_indx_stack.pop();
				}

				while (!bb_pdf_indx_stack.empty())
				{
					loc = gimple_location(
					          get_first_non_mpi_stmt(
					              get_bb_from_index(
					                  fun, bb_pdf_indx_stack.top())));
					inform (loc, "Diverging point might also be later at:");
					bb_pdf_indx_stack.pop();
				}
			}
		}
	}
}

basic_block pass_mpi_collective::get_bb_from_index(function *fun, int index)
{
	basic_block bb;

	FOR_ALL_BB_FN(bb, fun)
	{
		if (bb->index == index)
		{
			return bb;
		}
	}
	//should not happen
	return NULL;
}

//raise error if there is a collective order error
void pass_mpi_collective::raise_warning_mpi_order(function *fun)
{
	int mpi_code;
	int rank;

	bitmap_head nodes_head;
	bitmap nodes = &nodes_head;
	bitmap_initialize(nodes, &bitmap_default_obstack);

	int cursor_code;
	bitmap_iterator bi;
	unsigned int index;

	int curr_coll_rank;
	bool coll_rank_diff;
	int other_coll_rank;
	unsigned int diff_index;

	for (mpi_code = 0; mpi_code < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
	     ++mpi_code)
	{
		for (rank = 1; rank < collective_max_rank[mpi_code] + 1 ; rank++)
		{
			//printf("mpi_code: %i at rank: %i\n", mpi_code, rank);
			// DONE: split in 2, get mpi set & find set post dom
			get_mpi_coll_rank(fun, rank, mpi_code, nodes);
			//debug_bitmap(nodes);

			for (cursor_code = 0;
			     cursor_code < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
			     cursor_code++)
			{
				if (mpi_code == cursor_code)
				{
					continue;
				}

				//other collective being tested against
				curr_coll_rank = -1;
				//idx at which another collective call is found before
				diff_index = -1;
				coll_rank_diff = false;

				EXECUTE_IF_SET_IN_BITMAP(nodes, 0, index, bi)
				{

					other_coll_rank =
					    ((bb_data *) get_bb_from_index(fun, index)->aux)
					    ->collective_rank[cursor_code];
					//printf("bb %i as mpi code %i and mpi rank %i\n",
					//index, cursor_code, other_coll_rank);
					if (curr_coll_rank == -1)
					{
						curr_coll_rank = other_coll_rank;
					}
					else if (curr_coll_rank != other_coll_rank)
					{
						coll_rank_diff = true;
						diff_index = index;
					}
				}

				if (coll_rank_diff)
				{
					//printf("======> WARNING !!! ====> MPI "
					//		 "Collective rank missmatch for collective %s"
					//		 " at MPI Collective %s at rank %i\n",
					//		 mpi_collective_name[cursor_code],
					//		 mpi_collective_name[mpi_code], rank);

					location_t loc = gimple_location(
					                     get_mpi_stmt(
					                         get_bb_from_index(
					                             fun, diff_index)));

					warning_at(loc
					           , 0
					           , "MPI Collective %<%s%>(%i) might not be "
					           "reachable due to a precedent %<%s%> call:"
					           , mpi_collective_name[mpi_code]
					           , rank, mpi_collective_name[cursor_code]);

					EXECUTE_IF_SET_IN_BITMAP(nodes, 0, index, bi)
					{
						if (index != diff_index)
						{
							loc = gimple_location(
							          get_mpi_stmt(
							              get_bb_from_index(fun, index)));
							inform(loc
							       , "reachable %<%s%>(%i) without "
							       "a prior %<%s%> call:"
							       , mpi_collective_name[mpi_code]
							       , rank, mpi_collective_name[cursor_code]);
						}
					}
				}

			}

			bitmap_clear(nodes);
		}
	}
}
