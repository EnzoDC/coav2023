#include <cstdio>
// basename
#include <filesystem>

#include "mpi_collective.hpp"

/* Build a filename (as a string) based on function name */
char *pass_mpi_collective::cfgviz_generate_filename(function *fun,
        const char *suffix)
{
	char *target_filename;
	int line;

	target_filename = (char *)xmalloc(1024 * sizeof(char));
	line = LOCATION_LINE(fun->function_start_locus);
	const char *mfilename =
	    std::filesystem::path(LOCATION_FILE(fun->function_start_locus))
	    .filename().c_str();

	snprintf(target_filename, 1024, "%s%s_%s_%d_%s.dot",
	         "dot/",
	         mfilename,
	         function_name(fun),
	         line,
	         suffix);

	return target_filename;
}

/* Dump the graphviz representation of function 'fun' in file 'out' */
void pass_mpi_collective::cfgviz_internal_dump(function *fun, FILE *out)
{
	basic_block bb;
	edge_iterator eit;
	edge e;

	bitmap_iterator bi;
	unsigned int index;

	fprintf(out, "Digraph G{\n");
	FOR_ALL_BB_FN(bb, fun)
	{
		bb_data *data = ((bb_data *) bb->aux);

		fprintf(out, "%d [label=\"BB %d", bb->index, bb->index);
		if (data->mpi_code != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE)
		{
			fprintf(out, "|%s (%d) ", mpi_collective_name[data->mpi_code],
			        data->collective_rank[data->mpi_code]);
		}

		if (! bitmap_empty_p(&data->dom))
		{
			fprintf(out, "|dom(");

			EXECUTE_IF_SET_IN_BITMAP(&data->dom, 0, index, bi)
			{
				fprintf(out, "%d,", index);
			}
			fprintf(out, ")");
		}

		if (! bitmap_empty_p(&data->post_dom))
		{
			fprintf(out, "|post_dom(");
			EXECUTE_IF_SET_IN_BITMAP(&data->post_dom, 0, index, bi)
			{
				fprintf(out, "%d,", index);
			}
			fprintf(out, ")");
		}

		if (! bitmap_empty_p(&data->dom_front))
		{
			fprintf(out, "|dom_front(");
			EXECUTE_IF_SET_IN_BITMAP(&data->dom_front, 0, index, bi)
			{
				fprintf(out, "%d,", index);
			}
			fprintf(out, ")");
		}

		if (! bitmap_empty_p(&data->post_dom_front))
		{
			fprintf(out, "|post_dom_front(");
			EXECUTE_IF_SET_IN_BITMAP(&data->post_dom_front, 0, index, bi)
			{
				fprintf(out, "%d,", index);
			}
			fprintf(out, ")");
		}

		fprintf(out, "\" shape=ellipse]\n");

		FOR_EACH_EDGE(e, eit, bb->succs)
		{
			const char *label = "";
			if (e->flags == EDGE_TRUE_VALUE)
			{
				label = "true";
			}
			else if (e->flags == EDGE_FALSE_VALUE)
			{
				label = "false";
			}

			if (((edge_data *) e->aux)->loop)
			{
				label = "remove_loop";
			}

			fprintf(out, "%d -> %d [color=red label=\"%s\"]\n",
			        bb->index, e->dest->index, label);
		}
	}
	fprintf(out, "}\n");
}

void pass_mpi_collective::cfgviz_dump(function *fun, const char *suffix)
{
	char *target_filename;
	FILE *out;

	target_filename = cfgviz_generate_filename(fun, suffix);

	//printf("[GRAPHVIZ] Generating CFG of function %s in file <%s>\n",
	//       current_function_name(), target_filename);

	out = fopen(target_filename, "w");

	cfgviz_internal_dump(fun, out);

	fclose(out);
	free(target_filename);
}
