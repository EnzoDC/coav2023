#include <cstdio>

// dependence of c-pragma.h
#include <gcc-plugin.h>
// c_register_pragma
#include <c-family/c-pragma.h>
// IDENTIFIER_POINTER
#include <tree.h>
//// vec
//#include <vec.h>
//gcc error
#include <diagnostic.h>

#include "mpi_collective.hpp"

void register_mpicoll_check_pragma(void *event_data, void *data)
{
	(void) event_data;
	(void) data;
	//printf("registering pragma: \"ProjectCA\"\n");
	c_register_pragma("ProjetCA", "mpicoll_check", mpicoll_check_pragma);
}

//simple function to print fun_vec elements.
void print_fun_vec()
{
	tree x;
	for (unsigned int ix = 0; fun_vec->iterate(ix, &x); ix++)
	{
		printf("%s ", IDENTIFIER_POINTER(x));
	}
	printf("\n%i\n", fun_vec->length());
}

//add x if not present in fun_vec
void add_to_fun_vec(tree x, location_t loc)
{
	//verify if a newly added name is not already present in fun_vec->
	//raise warning if so.
	if (fun_vec->contains(x))
	{
		warning_at(loc, 0
		           , "repetition of function name %<%s%>"
		           " in %<#pragma ProjetCA mpicoll_check%>"
		           , IDENTIFIER_POINTER(x));
		return;
	}
	fun_vec->safe_push(x);
	loc_vec->safe_push(loc);
}

//By "taking inspiration", I just cc/cv and modified what was needed for
//our needs.
void mpicoll_check_pragma(cpp_reader *)
{
	location_t loc;
	enum cpp_ttype token;
	tree x;
	bool close_paren_needed_p = false;

	//not possible to use pragma inside function
	if (cfun)
	{
		error_at(cfun->function_start_locus
		         , "%<#pragma ProjetCA mpicoll_check%> "
		         "is not allowed inside functions");
		return;
	}

	token = pragma_lex (&x, &loc);
	if (token == CPP_OPEN_PAREN)
	{
		close_paren_needed_p = true;
		token = pragma_lex (&x, &loc);
	}

	if (token != CPP_NAME)
	{
		error_at(loc, "%<#pragma ProjetCA mpicoll_check%>"
		         " is not a valid name");
	}

	/* Strings are user options.  */
	else
	{
		do
		{
			/* Build up the strings now as a tree linked list.  Skip empty
			   strings.  */
			if (IDENTIFIER_LENGTH (x) > 0)
				//IDENTIFIER_LENGTH works
				//just fine for just checking if we have
				//a non-null token thingy here
			{
				if (token != CPP_NAME)
				{
					//turns out this never happens..
					warning_at(loc, 0, "Unrecognized name %s",
					           IDENTIFIER_POINTER(x));
				}
				//inform (loc, "Registering name %s", IDENTIFIER_POINTER(x));
				add_to_fun_vec(x, loc);
				//print_fun_vec();
			}

			token = pragma_lex (&x, &loc);
			while (token == CPP_COMMA)
			{
				token = pragma_lex (&x, &loc);
			}
		}
		while (token == CPP_NAME);

		if (close_paren_needed_p)
		{
			if (token == CPP_CLOSE_PAREN)
			{
				token = pragma_lex (&x, &loc);
			}
			else
			{
				error("%<#pragma ProjetCA mpicoll_check "
				      "(string [,string]...)%> does not have a final %<)%>");
			}
		}

		if (token != CPP_EOF)
		{
			error_at(loc
			         , "%<#pragma ProjetCA mpicoll_check%>"
			         " string is badly formed");
			return;
		}
	}
}

void verify_mpicoll_list(void *event_data, void *data)
{
	(void) event_data;
	(void) data;
	//printf("There are %i elm left in fun_vec\n",fun_vec->length());

	//if there a still functions in fun_vec, it means it was not found
	//while processing ; warn user about that.
	tree x;
	if (!fun_vec->is_empty())
	{
		for (unsigned int ix = 0; fun_vec->iterate(ix, &x); ix++)
		{
			warning_at((*loc_vec)[ix], 0
			           , "function %<%s%> was declared in a"
			           " %<#pragma ProjetCA mpicoll_check%>"
			           " but not found in file",
			           IDENTIFIER_POINTER(x));
		}
	}
	fun_vec->release();
	loc_vec->release();

	delete fun_vec;
	delete loc_vec;
}
