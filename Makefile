#CC=gcc
#CXX=g++

CC=gcc_1220
CXX=g++_1220

MPICC=mpicc

FLAGS=-Wall -Wextra -Werror -fPIC -fno-rtti
TEST_FLAGS=-Wall -Wextra
LDFLAGS=-shared

INCLUDE=-I ./include
PLUGIN_INCLUDE=$(INCLUDE) -I $(shell $(CC) -print-file-name=plugin)/include

# Options dependant flags
OPTI_FLAGS=-O2
DEBUG_FLAGS=-g -Og
DEBUG_TEST_FLAGS=-fdump-tree-all -fdump-tree-all-graph

# Comment to disable debug
FLAGS+=$(DEBUG_FLAGS)
#TEST_FLAGS+=$(DEBUG_TEST_FLAGS)
#GDB_GCC=-wrapper gdb,--args


PLUGIN_NAME=mpicoll_check

# Directories
SRCD=src
OBJD=obj
LIBD=lib
TARGETD=bin
TESTD=test
TESTSRCD=$(SRCD)/test
DOTD=dot

# Files
SRCS=$(notdir $(wildcard $(SRCD)/*.cpp))
OBJS=$(addprefix $(OBJD)/, $(SRCS:cpp=o))
TARGET=$(LIBD)/$(PLUGIN_NAME).so


PLUGIN_FLAGS=-fplugin=$(TARGET) #-fplugin-arg-$(PLUGIN_NAME)-dumpgraph -fplugin-arg-$(PLUGIN_NAME)-showinfo


.PHONY: all
all:
	$(MAKE) plugin
	$(MAKE) test
	$(MAKE) dots

.PHONY: plugin
plugin:
	mkdir -p $(OBJD)
	mkdir -p $(LIBD)
	$(MAKE) $(TARGET)

.PHONY: test
test:
	mkdir -p $(TESTD)
	mkdir -p $(DOTD)
	echo -e "\n"
	$(MAKE) $(TESTD)/test
	echo -e "\n"
	$(MAKE) $(TESTD)/test2
	echo -e "\n"
	$(MAKE) $(TESTD)/test3
	echo -e "\n"
	$(MAKE) $(TESTD)/test4
	echo -e "\n"
	$(MAKE) $(TESTD)/test_pragma1
	echo -e "\n"
	$(MAKE) $(TESTD)/test_pragma2
	echo -e "\n"

.PHONY: dots
dots:
	touch $(DOTD)/lock.dot
	$(MAKE) $(shell find $(DOTD) -name "*.dot" | sed 's/\.dot/\.svg/') \
	$(DOTD)/lock.svg

$(TESTD)/%: $(TESTSRCD)/%.c
	$(MPICC) $(GDB_GCC) $(TEST_FLAGS) $(PLUGIN_FLAGS) $^ -o $@ || echo "(exit 1)"

$(OBJD)/%.o: $(SRCD)/%.cpp 
	$(CXX) $(FLAGS) $(PLUGIN_INCLUDE) -c $^ -o $@

$(TARGET): $(OBJS)
	$(CXX) $(FLAGS) $(LDFLAGS) $^ -o $@
	# removing test scince plugin was rebuild
	rm -rf $(TESTD)

%.svg: %.dot
	  dot -Tsvg $< -o $@

.PHONY: clean
clean:
	rm -rf $(OBJD)
	rm -rf $(TARGETD)
	rm -rf $(LIBD)
	rm -rf $(TESTD)
	rm -rf $(DOTD)

.PHONY: retest
retest:
	rm -rf $(TESTD)
	$(MAKE) test

.PHONY: style
style:
	astyle $(SRCD)/*.cpp ./include/*.hpp -n -A1 -p -xj -J -xg -H -k3 -O -xC80 \
	-t4 -xt1 -m0
